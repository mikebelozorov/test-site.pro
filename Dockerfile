FROM yiisoftware/yii2-php:7.4-apache
MAINTAINER MikeBelozorov <mike@belozorov63.ru>


ARG CI_BUILD_NUM=dev-master
ARG CI_BRANCH=unknown
ARG CI_BUILD_DATE=1970-01-01
ARG CI_PROJECT=devproject

ENV LANG=en_US.UTF-8 \
    CI_BUILD_NUM=${CI_BUILD_NUM} \
    CI_BRANCH=${CI_BRANCH} \
    CI_PROJECT=${CI_PROJECT} \
    CI_BUILD_DATE=${CI_BUILD_DATE} \
    DEPS="git" \
    ROOT_PATH=/app/web \
    APACHE_LOG_DIR=/var/log

RUN set -xe \
 && apt-get update \
 && apt-get install -y $DEPS cron \
 && curl -sLo /usr/local/bin/ep https://github.com/kreuzwerker/envplate/releases/download/v0.0.8/ep-linux \
 && chmod +x /usr/local/bin/ep
 
RUN pecl install apcu_bc \
 && docker-php-ext-enable apcu

COPY docker/000-default.conf /etc/apache2/sites-available
COPY docker/migrate.sh /usr/local/bin
COPY docker/entrypoint.sh /usr/local/bin

COPY ./app/composer.* /app/
RUN composer install

COPY ./app /app

ENTRYPOINT [ "entrypoint.sh" ]
CMD [ "/usr/local/bin/apache2-foreground" ]


