<?php

use app\components\db\StrictActiveRecord;
use app\components\websocket\WebsocketComponent;
use yii\db\ActiveQuery;
use yii\gii\generators\controller\Generator as ControllerGenerator;
use yii\gii\generators\crud\Generator as CrudGenerator;
use yii\gii\generators\extension\Generator as ExtensionGenerator;
use yii\gii\generators\form\Generator as FormGenerator;
use yii\gii\generators\model\Generator as ModelGenerator;
use yii\gii\generators\module\Generator as ModuleGenerator;
use yii\web\Controller;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => getenv('REDIS_HOST'),
            'port' => getenv('REDIS_PORT'),
            'database' => getenv('REDIS_DATABASE'),
        ],
        'websocket' => [
            'class' => WebsocketComponent::class,
            'wsHost' => getenv('WS_HOST'),
        ],
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'model' => [
                'class' => ModelGenerator::class,
                'enableI18N' => true,
                'ns' => 'app\models',
                'baseClass' => StrictActiveRecord::class,
                'generateQuery' => true,
                'queryNs' => 'app\models\query',
                'queryBaseClass' => ActiveQuery::class,
            ],
            'crud' => [
                'class' => CrudGenerator::class,
                'enablePjax' => true,
                'enableI18N' => true,
                'baseControllerClass' => Controller::class,
            ],
            'controller' => ['class' => ControllerGenerator::class],
            'form' => ['class' => FormGenerator::class],
            'module' => ['class' => ModuleGenerator::class],
            'extension' => ['class' => ExtensionGenerator::class],
        ],
    ];
}

return $config;

