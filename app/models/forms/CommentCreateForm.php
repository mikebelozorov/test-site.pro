<?php

namespace app\models\forms;

use app\components\websocket\WebsocketComponent;
use app\models\Comment;
use Yii;
use yii\base\Model;

class CommentCreateForm extends Model
{
    public $name;
    public $text;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['name', 'text'], 'required'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function save()
    {
        if ($this->validate()) {
            $model = new Comment();
            $model->name = $this->name;
            $model->text = $this->text;
            $model->status = Comment::STATUS_ACTIVE;
            $result = $model->save();
            if ($result) {
                Yii::$app->websocket->emit(
                    WebsocketComponent::CHANNEL_COMMENT_NEW,
                    'User ' . $this->name . ' posted a new comment'
                );
            }
            return $result;
        }
        return false;
    }
}
