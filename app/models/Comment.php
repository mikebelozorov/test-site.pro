<?php

namespace app\models;

use app\components\websocket\WebsocketComponent;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property int $status
 * @property string|null $createdAt
 * @property string|null $updatedAt
 * @property int|null $updatedBy
 * @property int|null $createdBy
 */
class Comment extends \app\components\db\StrictActiveRecord
{
    public const STATUS_ACTIVE = 1;
    public const STATUS_DELETED = 10;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'text', 'status'], 'required'],
            [['text'], 'string'],
            [['status', 'updatedBy', 'createdBy'], 'integer'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['name'], 'string', 'max' => 255],

            [['status'], 'in', 'range' => [static::STATUS_ACTIVE, static::STATUS_DELETED]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'text' => Yii::t('app', 'Text'),
            'status' => Yii::t('app', 'Status'),
            'createdAt' => Yii::t('app', 'Created At'),
            'updatedAt' => Yii::t('app', 'Updated At'),
            'updatedBy' => Yii::t('app', 'Updated By'),
            'createdBy' => Yii::t('app', 'Created By'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => 'createdBy',
                'createdByAttribute' => 'updatedBy',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ]
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        Yii::$app->websocket->emit(WebsocketComponent::CHANNEL_COMMENTS_UPDATED, 1);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\CommentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\CommentQuery(get_called_class());
    }
}
