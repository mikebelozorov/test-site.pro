<?php

namespace app\models;

use app\components\behaviors\users\PasswordResetBehavior;
use app\components\db\StrictActiveRecord;
use app\models\query\UserQuery;
use app\validators\StrtolowerValidator;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 *
 * @property int $id
 * @property string $name
 * @property string $lastName
 * @property string $email
 * @property string $authKey
 * @property string $passwordHash
 * @property string $passwordResetToken
 * @property int $status
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $updatedBy
 * @property string $createdBy
 *
 * @property string $password write-only password
 *
 * @mixin TimestampBehavior
 * @mixin BlameableBehavior
 * @mixin PasswordResetBehavior
 */
class User extends StrictActiveRecord implements IdentityInterface
{
    public const STATUS_ACTIVE = 1;
    public const STATUS_DELETED = 10;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'user';
    }

    /**
     * @inheritdoc
     * @throws \yii\base\Exception
     */
    public function rules(): array
    {
        return [
            [['name', 'lastName', 'email'], 'string', 'max' => 255],
            [['name', 'lastName', 'email'], 'trim', 'skipOnEmpty' => true],
            [['email'], 'email'],
            [['email'], StrtolowerValidator::class],

            ['status', 'default', 'value' => User::STATUS_ACTIVE],
            ['status', 'in', 'range' => [User::STATUS_ACTIVE, User::STATUS_DELETED]],

            [['password'], 'string'],

            [['name', 'lastName', 'email', 'passwordHash', 'status'], 'required'],

            [['email', 'authKey'], 'unique'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'lastName' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'passwordHash' => Yii::t('app', 'Password Hash'),
            'passwordResetToken' => Yii::t('app', 'Password Reset Token'),
            'status' => Yii::t('app', 'Status'),
            'createdAt' => Yii::t('app', 'Created At'),
            'updatedAt' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields(): array
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'lastName' => 'lastName',
            'email' => 'email',
            'status' => 'status',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'createdBy',
                'updatedByAttribute' => 'updatedBy',
            ],
            'passwordResetToken' => [
                'class' => PasswordResetBehavior::class,
            ],
        ];
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     * @throws \yii\base\InvalidParamException
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->passwordHash);
    }

    /**
     * @param string $value
     * @throws \yii\base\Exception
     */
    public function setPassword(string $value)
    {
        if (!empty($value)) {
            $this->passwordHash = Yii::$app->security->generatePasswordHash($value);
        }
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => User::STATUS_ACTIVE, 'passwordResetToken' => null]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['email' => strtolower($username), 'status' => User::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find(): UserQuery
    {
        return new UserQuery(static::class);
    }
}
