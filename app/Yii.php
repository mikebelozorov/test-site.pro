<?php

/**
 * Yii bootstrap file.
 * Used for enhanced IDE code autocompletion.
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var AppApplication the application instance
     */
    public static $app;
}

/**
 * Class CommonApplication
 * Used for properties that are identical for both WebApplication and ConsoleApplication
 *
 * @property \yii\redis\Connection $redis
 * @property \app\components\websocket\WebsocketComponent $websocket
 */
abstract class AppApplication extends yii\base\Application
{
}
