<?php

namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

class CheckController extends Controller
{
    public function actionUserExist()
    {
        if (!User::find()->exists()) {
            Console::output('No one of users exist. Create first');
            $model = new User();
            $model->name = 'admin';
            $model->lastName = 'admin';
            $model->email = 'admin@admin.user';
            $model->password = '12345678';
            $model->status = User::STATUS_ACTIVE;
            $model->authKey = Yii::$app->security->generateRandomString();
            $model->save();

            // @note: for test
            $model->passwordResetToken = null;
            $model->save();
        }

        return ExitCode::OK;
    }
}
