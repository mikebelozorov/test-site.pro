<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

class WebsocketController extends Controller
{
    public function actionTest($channel = 'test', $message = 'hello world')
    {
        Yii::$app->websocket->emit($channel, $message);
    }
}
