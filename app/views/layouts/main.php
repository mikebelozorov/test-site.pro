<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\components\websocket\WebsocketComponent;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

/** @var \app\models\User $user */
$user = Yii::$app->user->identity;

Yii::$app->websocket->registerJs(
    WebsocketComponent::CHANNEL_TEST,
    new \yii\web\JsExpression('$.notify(msg);')
);

$items = [
    ['label' => 'Home', 'url' => '/'],
    ['label' => 'About', 'url' => ['/site/about']],
];
if (Yii::$app->user->isGuest) {
   $items[] = ['label' => 'Login', 'url' => ['/admin/auth/login']];
} else {
    $items[] = ['label' => 'Comments', 'url' => ['/admin/comment/index']];
    $items[] = '<li>'
    . Html::beginForm(['/admin/auth/logout'], 'post')
    . Html::submitButton(
        'Logout (' . $user->email . ')',
        ['class' => 'btn btn-link logout']
    )
    . Html::endForm()
    . '</li>';

    Yii::$app->websocket->registerJs(
        WebsocketComponent::CHANNEL_COMMENT_NEW,
        new \yii\web\JsExpression('$.notify(msg);')
    );
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
