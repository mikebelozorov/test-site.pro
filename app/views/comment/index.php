<?php

use app\components\websocket\WebsocketComponent;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\forms\CommentCreateForm */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Comments');
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->websocket->registerJs(
    WebsocketComponent::CHANNEL_COMMENTS_UPDATED,
    new \yii\web\JsExpression('$.pjax.reload({container: "#comments-list"});')
);
?>
<div class="comment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(['id' => 'comments-list']); ?>

    <?= $this->render('_form', ['model' => $model]); ?>


    <div class="panel panel-default">
        <div class="panel-heading">Publish comment</div>
        <div class="panel-body">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'itemView' => '_item',
            ]); ?>
        </div>
    </div>

    <?php Pjax::end(); ?>

</div>
