<?php

/* @var $this yii\web\View */
/* @var $model \app\models\Comment */

?>

<div class="panel panel-default">
    <div class="panel-heading">Comment #<?= $model->id ?></div>
    <div class="panel-body">
        <?= $model->text ?>
    </div>
    <div class="panel-footer">Name: <?= $model->name ?></div>
</div>