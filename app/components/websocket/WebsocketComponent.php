<?php

namespace app\components\websocket;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\base\Component;
use yii\di\Instance;
use yii\helpers\Json;
use yii\redis\Connection;
use yii\web\JsExpression;
use yii\web\View;

class WebsocketComponent extends Component implements BootstrapInterface
{
    public const CHANNEL_COMMENTS_UPDATED = 'comments_updated';
    public const CHANNEL_COMMENT_NEW = 'comment_new';
    public const CHANNEL_TEST = 'test';

    /**
     * @var string|Connection
     */
    public $redis = 'redis';

    public ?string $wsHost = null;
    public ?string $wsChannel = 'websocket';

    protected string $js;

    public function init()
    {
        parent::init();
        $this->redis = Instance::ensure($this->redis, Connection::class);

        $this->js = "window.ws = io('$this->wsHost', { transports: ['websocket'] });" . PHP_EOL;
    }

    public function emit(string $channel, $message)
    {
        $data = ['channel' => $channel, 'message' => $message];
        $this->redis->publish($this->wsChannel, Json::encode($data));
    }

    public function registerJs(string $channel, JsExpression $js)
    {
        $result[] = "ws.on('$channel', function(msg) {";
        $result[] = $js->expression;
        $result[] = '});';
        $this->js .= implode(PHP_EOL, $result);
    }

    public function bootstrap($app)
    {
        Yii::$app->view->on(View::EVENT_END_BODY, function () {
            Yii::$app->view->registerJs($this->js, View::POS_END);
        });
        WebSocketAsset::register(Yii::$app->view);
    }
}
