<?php

namespace app\components\websocket;

use app\assets\AppAsset;
use yii\web\AssetBundle;

class WebSocketAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';
    public $css = [
    ];
    public $js = [
        'ws.js',
    ];
    public $depends = [
        AppAsset::class,
    ];
}
