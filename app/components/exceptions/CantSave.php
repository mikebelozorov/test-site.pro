<?php

namespace app\components\exceptions;

use yii\base\Exception;
use yii\db\ActiveRecord;

class CantSave extends Exception
{
    /**
     * @var mixed model errors
     */
    protected $errors;

    /**
     * @var mixed model attributes
     */
    protected $attributes;

    /**
     * @param ActiveRecord $model
     * @param string $message
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($model, $message = "", $code = 0, \Exception $previous = null)
    {
        $this->errors = $model->errors;
        $this->attributes = $model->attributes;

        \Exception::__construct(
            'Can\'t save ' . get_class($model) . ': ' . print_r($this->errors, true) .
            'Attributes: ' . print_r($this->attributes, true),
            $code,
            $previous
        );
    }
}
