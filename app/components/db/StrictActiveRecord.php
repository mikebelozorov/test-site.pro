<?php

namespace app\components\db;

use app\components\exceptions\CantSave;
use yii\db\ActiveRecord;

class StrictActiveRecord extends ActiveRecord
{
    /**
     * @inheritdoc
     * @throws CantSave
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            throw new CantSave($this);
        }

        return $result;
    }
}
