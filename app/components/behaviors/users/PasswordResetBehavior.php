<?php

namespace app\components\behaviors\users;

use app\models\User;
use Yii;
use yii\base\Behavior;

/**
 * Class PasswordResetBehavior
 * @package app\components\behaviors\users
 *
 * @property User $owner
 */
class PasswordResetBehavior extends Behavior
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            User::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }

    /**
     * @throws \yii\base\Exception
     */
    public function beforeValidate()
    {
        if (get_class($this->owner) === User::class) {
            if ($this->owner->isNewRecord) {
                $this->generatePasswordResetToken();
            }
        }
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return User|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return User::findOne([
            'passwordResetToken' => $token,
            'status' => User::STATUS_ACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token): bool
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     * @throws \yii\base\Exception
     */
    public function generatePasswordResetToken()
    {
        $this->owner->passwordResetToken = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->owner->passwordResetToken = null;
    }
}
