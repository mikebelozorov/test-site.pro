<?php

namespace app\components\serializers;

use Yii;
use yii\base\Arrayable;
use yii\base\Model;
use yii\data\DataProviderInterface;

/**
 * Class Serializer
 * @package api\components\serializers
 */
class Serializer extends \yii\rest\Serializer
{
    /**
     * @inheritdoc
     */
    public function serialize($data)
    {
        if ($data instanceof Model && $data->hasErrors()) {
            $result =  $this->serializeModelErrors($data);
            Yii::warning($result);
            return $result;
        } elseif ($data instanceof Arrayable) {
            return $this->serializeModel($data);
        } elseif ($data instanceof DataProviderInterface) {
            return $this->serializeDataProvider($data);
        }

        return $data;
    }
}
