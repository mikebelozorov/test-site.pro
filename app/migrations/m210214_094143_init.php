<?php

use yii\db\Migration;

/**
 * Class m210214_094143_init
 */
class m210214_094143_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user', [
            'id' => $this->primaryKey(11)->unsigned(),
            'name' => $this->string()->notNull(),
            'lastName' => $this->string()->notNull(),
            'email' => $this->string()->unique()->notNull(),

            'authKey' => $this->string()->notNull()->unique(),
            'passwordHash' => $this->string()->notNull(),
            'passwordResetToken' => $this->string()->unique(),

            'status' => $this->smallInteger()->notNull(),

            'createdAt' => $this->dateTime(),
            'updatedAt' => $this->dateTime(),
            'updatedBy' => $this->integer()->unsigned(),
            'createdBy' => $this->integer()->unsigned(),
        ], $tableOptions);

        $this->createTable('comment', [
            'id' => $this->primaryKey(11)->unsigned(),
            'name' => $this->string()->notNull(),
            'text' => $this->text()->notNull(),

            'status' => $this->smallInteger()->notNull(),

            'createdAt' => $this->dateTime(),
            'updatedAt' => $this->dateTime(),
            'updatedBy' => $this->integer()->unsigned(),
            'createdBy' => $this->integer()->unsigned(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('comment');
        $this->dropTable('user');
    }
}
