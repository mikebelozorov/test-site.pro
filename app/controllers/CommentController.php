<?php

namespace app\controllers;

use app\models\Comment;
use app\models\forms\CommentCreateForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class CommentController extends Controller
{
    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Comment::find()
                ->andWhere(['status' => Comment::STATUS_ACTIVE])
                ->orderBy(['createdAt' => SORT_DESC])
        ]);

        $model = new CommentCreateForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }
}
