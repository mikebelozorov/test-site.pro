#!/bin/bash
set -e
set -x

./yii migrate --interactive=0
./yii check/user-exist
RET=$?
./yii cache/flush-schema --interactive=0
exit $RET
