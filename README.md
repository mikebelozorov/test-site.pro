# RUN

1. copy env file
    ```bash
    cp .env-example .env
    ```

2. edit .env file
    ```bash
    vi .env
    # or
    nano .env
    ```
   
3. run docker containers
    ```bash
    docker-compose up -d
    ```

4. open site in browser by forwarded port

5. _(if we use traefik)_ open [yii2-docker-template.docker](http://yii2-docker-template.docker) in your browser 
