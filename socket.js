var server = require('http').Server();

var io = require('socket.io')(server);

var Redis = require('ioredis');
var redis = new Redis(process.env.REDIS_PORT, process.env.REDIS_HOST);

io.on('connection', (socket) => {
  console.log('a user connected');
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });
});

redis.subscribe(process.env.WS_REDIS_CHANNEL, function () {
  console.log('Subscription successful');
});

redis.on('message', function(channel, message) {
  console.log(channel, message);

  message = JSON.parse(message);
  console.log(channel, message);
  io.emit(message.channel, message.message);
});

server.listen(3000);
