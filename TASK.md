###### Test for Full-Stack developer:

You need to create a simple commenting system (front-end and administration area) with the Yii framework:

**Front-end:** A person fills in a comment filed (makes a post) without logging in. The post appears in the comments feed.

**Administration area:** the administrator logs in and sees the list of comments (content, date, etc.), can edit the comment / delete it. When a new comment was posted in front-end - administrator sees it in administrative area's list of comments updated in realtime (web-sockets are used for this), also notification about new comment pops out.